<?php 
    $fruits = array('banana','apple','watermelon','orange','durian','grape'); 
?> 

<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Fruits.php</title>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>

<body style="font-size:18px">

    <div class="conytainer-fluid">
        <div class="container well-lg">
            <h2>Some fruits I like:</h2>
            <ul class="list-group">
                <?php foreach ($fruits as $fruit): ?>
                    <li class="list-group-item"> <?php echo $fruit ?> </li>
                <?php endforeach ?>
            </ul>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script></script>
</body>
</html>