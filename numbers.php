
<?php 
    $minNumber = 1;
    $coloumn = 7;
    $maxNumber =100;
?> 

<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Numbers.php</title>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>

<body style="font-size:18px">

    <div class="conytainer-fluid">
        <div class="container well-lg">
            <h2>Sample Output:</h2>
            <p>
                <?php for ($n=$minNumber; $n<=$maxNumber; $n++){
                    if( ($n-$minNumber+1) % $coloumn == 0){
                        echo $n . "<br>";
                    } else{
                        echo $n . ' ';
                    }
                }?>
            </p>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script></script>
</body>
</html>