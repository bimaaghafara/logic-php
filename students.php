<?php 
    $students = array('Peter'=>16,'John'=>17,'Greg'=>15,'Bob'=>18); 
?> 

<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Students.php</title>

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>

<body style="font-size:18px">

    <div class="conytainer-fluid">
        <div class="container well-lg">
            <h2>List of students:</h2>
            <ul class="list-group">
                <?php foreach ( array_reverse($students, true) as $name => $age): ?>
                    <li class="list-group-item"> 
                        <?php echo $name . ' is '. $age ?> years old 
                    </li>
                <?php endforeach ?>
            </ul>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script></script>
</body>
</html>